//
//  Background.swift
//  Buton_foto
//
//  Created by LaboratoriOS Cronian Academy on 28/03/2018.
//  Copyright © 2018 LaboratoriOS Cronian Academy. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addBackground() {
        // screen width and height:
        //_ = UIScreen.main.bounds.size.width
        //_ = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: UIScreen.main.bounds)
        imageViewBackground.image = UIImage(named: "background.jpg")
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubview(toBack: imageViewBackground)
    }}
